package br.com.gestaodecartoes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestaoDeCartoesApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestaoDeCartoesApplication.class, args);
	}

}
