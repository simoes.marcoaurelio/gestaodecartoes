package br.com.gestaodecartoes.models;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "Insira uma descrição válida para o pagamento")
    @Size(min = 2, max = 100, message = "A descrição do pagamento deve ter entre 2 e 100 caracteres")
    private String descricao;

    @DecimalMin(value = "0.1", message = "Insira um valor maior que zero para o pagamento")
    private double valor;

    @ManyToOne
    @NotNull(message = "O pagamento precisa estar atrelado a um cartão")
    private Cartao cartao;

    public Pagamento() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }
}
