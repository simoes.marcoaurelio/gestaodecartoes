package br.com.gestaodecartoes.repositories;

import br.com.gestaodecartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    Iterable<Pagamento> findAllByCartao_id(int cartao_id);
}
