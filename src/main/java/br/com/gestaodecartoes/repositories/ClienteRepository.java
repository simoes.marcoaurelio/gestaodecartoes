package br.com.gestaodecartoes.repositories;

import br.com.gestaodecartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente,Integer> {
}
