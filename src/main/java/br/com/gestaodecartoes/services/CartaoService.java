package br.com.gestaodecartoes.services;

import br.com.gestaodecartoes.dtos.CadastroCartaoDTO;
import br.com.gestaodecartoes.dtos.ConsultaCartaoDTO;
import br.com.gestaodecartoes.models.Cartao;
import br.com.gestaodecartoes.models.Cliente;
import br.com.gestaodecartoes.repositories.CartaoRepository;
import br.com.gestaodecartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    public CadastroCartaoDTO cadastrarNovoCartao(CadastroCartaoDTO cadastroCartaoDTO) {
        Optional<Cliente> optionalCliente = clienteRepository.findById(cadastroCartaoDTO.getClienteId());

        if (optionalCliente.isPresent()) {
            Cartao cartao = new Cartao();
            cartao.setNumero(cadastroCartaoDTO.getNumero());
            cartao.setAtivo(false);
            cartao.setCliente(optionalCliente.get());
            cartaoRepository.save(cartao);

            cadastroCartaoDTO.setId(cartao.getId());
            cadastroCartaoDTO.setAtivo(cartao.isAtivo());

            return cadastroCartaoDTO;
        }
        else {
            throw new RuntimeException("Cliente não encontrado");
        }
    }

    public CadastroCartaoDTO ativarCartao(String numero, CadastroCartaoDTO cadastroCartaoDTO) {
        Optional<Cartao> optionalCartao = cartaoRepository.findByNumero(numero);

        if (optionalCartao.isPresent()) {
            Cartao cartao = optionalCartao.get();
            cartao.setAtivo(cadastroCartaoDTO.isAtivo());
            cartaoRepository.save(cartao);

            cadastroCartaoDTO.setId(cartao.getId());
            cadastroCartaoDTO.setNumero(numero);
            cadastroCartaoDTO.setAtivo(cartao.isAtivo());
            cadastroCartaoDTO.setClienteId(cartao.getCliente().getId());

            return cadastroCartaoDTO;
        }
        else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public ConsultaCartaoDTO consultarCartaoPeloNumero(String numero) {
        Optional<Cartao> optionalCartao = cartaoRepository.findByNumero(numero);

        if (optionalCartao.isPresent()) {
            Cartao cartao = optionalCartao.get();

            ConsultaCartaoDTO consultaCartaoDTO = new ConsultaCartaoDTO();
            consultaCartaoDTO.setId(cartao.getId());
            consultaCartaoDTO.setNumero(cartao.getNumero());
            consultaCartaoDTO.setClienteId(cartao.getCliente().getId());

            return consultaCartaoDTO;
        }
        else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public boolean consultarExistenciaDeCartao(CadastroCartaoDTO cadastroCartaoDTO) {
        Optional<Cartao> optionalCartao = cartaoRepository.findByNumero(cadastroCartaoDTO.getNumero());

        if (optionalCartao.isPresent()) {
            throw new RuntimeException("Esse cartão já existe em nosso cadastro");
        }
        else {
            return false;
        }
    }
}
