package br.com.gestaodecartoes.services;

import br.com.gestaodecartoes.dtos.PagamentoDTO;
import br.com.gestaodecartoes.models.Cartao;
import br.com.gestaodecartoes.models.Pagamento;
import br.com.gestaodecartoes.repositories.CartaoRepository;
import br.com.gestaodecartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoRepository cartaoRepository;

    public PagamentoDTO incluirNovoPagamento(PagamentoDTO pagamentoDTO) {
        Optional<Cartao> optionalCartao = cartaoRepository.findById(pagamentoDTO.getCartao_id());

        if (optionalCartao.isPresent()) {
            Cartao cartao = optionalCartao.get();

            Pagamento pagamento = new Pagamento();
            pagamento.setDescricao(pagamentoDTO.getDescricao());
            pagamento.setValor(pagamentoDTO.getValor());
            pagamento.setCartao(cartao);
            pagamentoRepository.save(pagamento);

            pagamentoDTO.setId(pagamento.getId());

            return pagamentoDTO;
        }
        else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public List<PagamentoDTO> buscarPagamentosPorCartao(int id_cartao) {
        Iterable<Pagamento> pagamentos = pagamentoRepository.findAllByCartao_id(id_cartao);

        List<PagamentoDTO> listaDePagamentos = new ArrayList<>();
        PagamentoDTO pagamentoDTO;

        int contador = 0;

        for (Pagamento pagamento : pagamentos) {
            pagamentoDTO = new PagamentoDTO();
            pagamentoDTO.setId(pagamento.getId());
            pagamentoDTO.setCartao_id(id_cartao);
            pagamentoDTO.setDescricao(pagamento.getDescricao());
            pagamentoDTO.setValor(pagamento.getValor());

            listaDePagamentos.add(pagamentoDTO);
            contador++;
        }

        if (contador > 0) {
            return listaDePagamentos;
        }
        else {
            throw new RuntimeException("Não existem pagamentos para esse cartão");
        }
    }
}
