package br.com.gestaodecartoes.services;

import br.com.gestaodecartoes.dtos.CadastroCartaoDTO;
import br.com.gestaodecartoes.dtos.ClienteDTO;
import br.com.gestaodecartoes.models.Cartao;
import br.com.gestaodecartoes.models.Cliente;
import br.com.gestaodecartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public ClienteDTO cadastrarNovoCliente(Cliente cliente) {
        clienteRepository.save(cliente);

        ClienteDTO clienteDTO = new ClienteDTO();
        clienteDTO = movimentarClienteParaClientedto(cliente, clienteDTO);

        return clienteDTO;
    }

    public ClienteDTO consultarClientePorId(int id) {
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);

        if (optionalCliente.isPresent()) {
            Cliente cliente = optionalCliente.get();

            ClienteDTO clienteDTO = new ClienteDTO();
            clienteDTO = movimentarClienteParaClientedto(cliente, clienteDTO);

            return clienteDTO;
        }
        else {
            throw new RuntimeException("Cliente não cadastrado");
        }
    }

    public ClienteDTO movimentarClienteParaClientedto(Cliente cliente, ClienteDTO clienteDTO) {
        clienteDTO.setId(cliente.getId());
        clienteDTO.setName(cliente.getName());

        return clienteDTO;
    }
}
