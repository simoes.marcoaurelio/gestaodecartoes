package br.com.gestaodecartoes.controllers;

import br.com.gestaodecartoes.dtos.PagamentoDTO;
import br.com.gestaodecartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoDTO incluirNovoPagamento(@RequestBody @Valid PagamentoDTO pagamentoDTO) {
        try {
            return pagamentoService.incluirNovoPagamento(pagamentoDTO);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

    @GetMapping("/pagamentos/{id_cartao}")
    @ResponseStatus(HttpStatus.OK)
    public List<PagamentoDTO> buscarPagamentosPorCartao(@PathVariable int id_cartao) {
        try {
            return pagamentoService.buscarPagamentosPorCartao(id_cartao);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,exception.getMessage());
        }
    }
}
