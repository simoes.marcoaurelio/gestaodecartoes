package br.com.gestaodecartoes.controllers;

import br.com.gestaodecartoes.dtos.CadastroCartaoDTO;
import br.com.gestaodecartoes.dtos.ConsultaCartaoDTO;
import br.com.gestaodecartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CadastroCartaoDTO cadastrarNovoCartao(@RequestBody @Valid CadastroCartaoDTO cadastroCartaoDTO) {
        try {
            Boolean cartaoExistente = cartaoService.consultarExistenciaDeCartao(cadastroCartaoDTO);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,exception.getMessage());
        }

        try {
            return cartaoService.cadastrarNovoCartao(cadastroCartaoDTO);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PatchMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public CadastroCartaoDTO ativarCartao(@PathVariable String numero,
                                          @RequestBody @Valid CadastroCartaoDTO cadastroCartaoDTO) {
        try {
            return cartaoService.ativarCartao(numero, cadastroCartaoDTO);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

    @GetMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public ConsultaCartaoDTO consultarCartaoPeloNumero(@PathVariable String numero) {
        try {
            return cartaoService.consultarCartaoPeloNumero(numero);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }
}
