package br.com.gestaodecartoes.controllers;

import br.com.gestaodecartoes.dtos.ClienteDTO;
import br.com.gestaodecartoes.models.Cliente;
import br.com.gestaodecartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClienteDTO cadastrarNovoCliente(@RequestBody @Valid Cliente cliente) {
        try {
            return clienteService.cadastrarNovoCliente(cliente);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ClienteDTO consultarClientePorId(@PathVariable int id) {
        try {
            ClienteDTO clienteDTO = clienteService.consultarClientePorId(id);
            return clienteDTO;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }
}
